
const langJs = document.querySelector('.lang-js'),
langJsM = document.querySelector('.lang-js-m'),
langJsList = document.querySelector('.lang-js-list'),
langJsListM = document.querySelector('.lang-js-list-m'),
langDropWrap = document.querySelector('.lang-drop-wrap-d'),
langDropWrapM = document.querySelector('.lang-drop-wrap-m'),
langTopJsSpan = document.querySelector('.lang-top-js span'),
langTopJsSpanM = document.querySelector('.lang-top-js-m span'),
closeJs = document.querySelector('.close'),
menuMJs = document.querySelector('.menu-m-js');

$(function () {
    $("a[href^='#']").click(function () {
        var _href = $(this).attr("href");
        $("html, body").animate({scrollTop: $(_href).offset().top - 55 + "px"}, 1500);
        return false;
    });
});

menuMJs.addEventListener('click', () => {
    menuMJs.classList.add('active');
});

closeJs.addEventListener('click', () => {
    setTimeout(() => {
        menuMJs.classList.remove('active');
    }, 100);
    
 
});

langJs.addEventListener('click', (e) => {
    
    if (e.target.closest('.lang-drop')) {
        
    } else {
        langJs.classList.toggle('active');
    }
 
    document.body.addEventListener('click', (e) => {
        if (!e.target.closest('.lang-js')) {
            langJs.classList.remove('active');
        } else {
            langJs.classList.add('active');
        }
    });
 
  
});
langJsM.addEventListener('click', (e) => {
    
    if (e.target.closest('.lang-drop')) {
        
    } else {
        langJsM.classList.toggle('active');
    }
 
    document.body.addEventListener('click', (e) => {
        if (!e.target.closest('.lang-js-m')) {
            langJsM.classList.remove('active');
        } else {
            langJsM.classList.add('active');
        }
    });
 
  
});

langJsList.addEventListener('click', (e) => {
    e.preventDefault();
    langJsList.classList.toggle('active');
    document.body.addEventListener('click', (e) => {
        if (!e.target.closest('.lang-js-list')) {
            langJsList.classList.remove('active');
        } else {
            langJsList.classList.add('active');
        }
    });
});

langJsListM.addEventListener('click', (e) => {
    e.preventDefault();
    langJsListM.classList.toggle('active');
    document.body.addEventListener('click', (e) => {
        if (!e.target.closest('.lang-js-list-m')) {
            langJsListM.classList.remove('active');
        } else {
            langJsListM.classList.add('active');
        }
    });
});

langDropWrap.addEventListener('click', (e) => {
    e.preventDefault();
    if (e.target.closest('.lang-drop-wrap a')) {
        langTopJsSpan.textContent = e.target.closest('.lang-drop-wrap-d a').textContent;
        
        setTimeout(() => {
            langJsList.classList.remove('active');
           
        }, 100);
    }
});
langDropWrapM.addEventListener('click', (e) => {
    e.preventDefault();
    if (e.target.closest('.lang-drop-wrap-m a')) {
        langTopJsSpanM.textContent = e.target.closest('.lang-drop-wrap-m a').textContent;
        
        setTimeout(() => {
            langJsListM.classList.remove('active');
           
        }, 100);
    }
});

