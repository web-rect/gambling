const contJs = document.querySelector('.cont-js'),
articJs = document.querySelectorAll('.artic-js'),
articTool = document.querySelectorAll('.artic-tool'),
articContentJs = document.querySelector('.artic-content-js'),
tabJs = document.querySelector('.tab-js'),
tabJsA = document.querySelectorAll('.tab-js a'),
formTab = document.querySelectorAll('.form-tab'),
formDis = document.querySelector('.form-dis'),
contentsLangJs = document.querySelectorAll('.contents-lang-js .box-search'),
formInputDrop = document.querySelector('.drop-target'),
formInputDropDwn = document.querySelector('.form-unput-drop-dwn'),
imgDrop = document.querySelector('.img-drop');



const openArtic = (e) => {
    const tar = e.target.closest('.artic-js');
    if (tar) {

      
        if (tar.getAttribute('name')) {

            if (e.target.closest('.artic-vis')) {
                tar.classList.remove('active');
                tar.removeAttribute('name', 'name');
            }
           
        } else {

            articJs.forEach(function(item){
     
                item.classList.remove('active');
                item.removeAttribute('name', 'name');
    
            });

           
            tar.classList.add('active');
            tar.setAttribute('name', 'name');
            
            
        }


    }
};

const openArticlTool = (e) => {

    const tar = e.target.closest('.artic-tool');
    if (tar) {

      
        if (tar.getAttribute('name')) {
            
            if (e.target.closest('.artic-tool-vis')) {
                tar.classList.remove('active-tool');
                tar.removeAttribute('name', 'name');
            }
           
        } else {

            articTool.forEach(function(item){
     
                item.classList.remove('active-tool');
                item.removeAttribute('name', 'name');
    
            });

           
            tar.classList.add('active-tool');
            tar.setAttribute('name', 'name');
            
            
        }


    }
};

const tabIn = (event) => {
    
    event.preventDefault();
    const target = event.target;
   
    if (target.closest('a')) {
        tabJsA.forEach(function (item) {
            item.classList.remove('active');
            formTab.forEach(element => {
                element.classList.remove('active');
                if (element.dataset.tab === target.dataset.tab) {
                    target.classList.add('active');
                    element.classList.add('active');
                }
            });
        })
    }

};

function dropList (event) {
    event.preventDefault();

    formInputDropDwn.classList.toggle('active');

    contentsLangJs.forEach(function(item) {

            const spanItemImg = item.querySelector('img');
            const spanItem = item.querySelector('span');
            // formInputDrop.textContent = '';
            item.addEventListener('click', () => {
                formInputDrop.textContent = spanItem.textContent;
                formDis.value = spanItem.textContent;
                formInputDrop.insertAdjacentHTML('beforeend', `<img src="/img/country/drop.png" alt="">`);

                imgDrop.src = spanItemImg.src;
                formInputDropDwn.classList.remove('active');
            });

    });
   
};

contJs.addEventListener('click', openArtic);

articContentJs.addEventListener('click', openArticlTool);
tabJs.addEventListener('click', tabIn);
formDis.addEventListener('click', dropList);
