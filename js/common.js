$.fn.isVisible = function () {

	let elementTop = $(this).offset().top;
	let elementBottom = elementTop + $(this).outerHeight();

	let viewportTop = $(window).scrollTop();
	let viewportBottom = viewportTop + $(window).height();

	let flag = (elementBottom + 50) > viewportTop && (elementTop - 50) < viewportBottom;


	return flag;

};




	$(window).on('load scroll', function(){
		if($('.graph__content').isVisible()) {
			$('.graph__pane.active .graph__value').removeClass('graph__disabled');
		}
		if($('.start__img').isVisible()) {
			setTimeout(function(){
				$('.start__anim').addClass('active');
			}, 650);
		}
	})

	$('.graph__pane .graph__value').addClass('graph__disabled');
	$(document).on("click", ".graph__tab", function(e) {
		e.preventDefault();
		var 
		numberIndex = $(this).index(),
		id = $(this).attr('data-id');

		if (!$(this).hasClass("active")) {
			$('.graph__value').addClass('graph__disabled');
			$(".graph__tab").removeClass("active");
			$(".graph__pane").removeClass("active");


			$(this).addClass("active");
			$('#graph-'+id).addClass("active");

			setTimeout(function(){
				$('#graph-'+id).find(".graph__value").removeClass('graph__disabled');
			}, 150);

		}

		return false;
	});

	$('.graph__list').each(function(){

		var $list = $(this);
		var $max = 0;
		var $items = [];

		$list.find('.graph__item').each(function(key, elem){
			var $val = parseInt($(elem).attr('data-val'));
			if($val > $max) {$max = $val;}
			$items[key] = $val;
		})

		$list.find('.graph__item').each(function(key, elem){
			$(elem).find('.graph__value').css('max-height', ($items[key] / $max * 100) + '%');
		})

	});

	$(window).on('resize', function(){$.fn.matchHeight._update();});


	$('.goTo').click(function(){
		var target = $(this).attr('href');
		$('html, body').animate({scrollTop: $(target).offset().top}, 800);
		return false;
	});
