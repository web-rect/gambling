const faqJs = document.querySelector('.faq-js'),
faqBoxJs = document.querySelectorAll('.faq-box-js');



const faqActive = (e) => {
    const tar = e.target.closest('.faq-box-js');
    if (tar) {

      
        if (tar.getAttribute('name')) {

            if (e.target.closest('.top-text')) {
                tar.classList.remove('active');
                tar.removeAttribute('name', 'name');
            }
           
        } else {

            faqBoxJs.forEach(function(item){
     
                item.classList.remove('active');
                item.removeAttribute('name', 'name');
    
            });

           
            tar.classList.add('active');
            tar.setAttribute('name', 'name');
            
            
        }


    }
};



faqJs.addEventListener('click', faqActive);