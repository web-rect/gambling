
const $modalFormQuiz = document.querySelector('.modal-form-quiz'),
$btnModalJs = document.querySelectorAll('.btn-modal-js'),
$btnBlack = document.querySelectorAll('.btn-black');


if ($modalFormQuiz) {
    $btnModalJs.forEach(linkBtn => {
        linkBtn.addEventListener('click', e => {
            e.preventDefault();
            $modalFormQuiz.classList.add('active');
        });
    })
    $modalFormQuiz.addEventListener('click', e => {
        
        if (e.target.closest('.modal-form-quiz-bg') || e.target.closest('.close')) {
            $modalFormQuiz.classList.remove('active');
        }
    })

    let stcount = 0;
    const $items = $modalFormQuiz.querySelectorAll('.items'); 
    const $itemsError = document.querySelectorAll('.text-error'); 
    const slideGoBtn = document.querySelectorAll('.btn-js-go');
    const btnReturnОs = document.querySelector('.btn-return-js');

    let $checkAllInp = "";
    let $allInputVal = "";
    let $textarea = "";

    slideGoBtn.forEach((link, i) => {
        link.addEventListener('click', e => {

            let $itemsChekedInput = $items[0].querySelectorAll('input:checked').length;
            let $itemsValueInput = $items[1].querySelectorAll('input');


            errorRemove();

     
            if (e.target.closest('.bt1')) {
                if ($itemsChekedInput > 0) {
                    $items[0].querySelectorAll('input:checked').forEach(val => {
                        $checkAllInp += val.value + ', ';
                    })
                    console.log($items); 
                  
                    countplus();
                    errorRemove();
           
                }  else {
                    errorAdd();
                }
            }

          
            if (e.target.closest('.bt3')) {
                if ($itemsValueInput[0].value.length > 1 && $itemsValueInput[1].value.length > 1 || $itemsValueInput[2].value.length > 1) {
                    
                    $itemsValueInput.forEach(inps => {
                        $allInputVal += inps.value + ', ';
                    })

                    const formData = new FormData();
                    formData.append('type', $checkAllInp);
                    formData.append('textarea', $textarea);
                    formData.append('contact', $allInputVal);
            
                    const ftsend = (data) => {
                        fetch('send.php', {
                            method: 'POST',
                            body: data,
                        })
                        .then(resp => resp.text())
                        .then(res => {
                            countplus();
                            errorRemove();
                        });
                        
                    }
            
                    ftsend(formData);

                }  else {
                    errorAdd();
                }
            }
            if (e.target.closest('.btn-return-js')) {
                countplus();
                $modalFormQuiz.classList.remove('active');
                errorRemove();
            }

        });
    });



    $btnBlack.forEach((link, i) => {
        link.addEventListener('click', e => {
            errorRemove();
            countminus();
            
        });
      
    });
    const countplus = () => {

        if (slideGoBtn.length - 1 === stcount) {
            stcount = 0;
        } else {
            stcount++;

        }
      
        sliderModal();
    }
    
    const countminus = () => {

        stcount = stcount - 1;
        sliderModal();
    }

    const sliderModal = () => {
        for (let i = 0; i < $items.length; i++) {
         
            const element = $items[i];
            element.classList.remove('active');
            $items[stcount].classList.add('active');
        }
        
      
    }

    const errorAdd = () => {
        $itemsError.forEach(item => {
            item.classList.add('active');
        });
    }
    const errorRemove = () => {
        $itemsError.forEach(item => {
            item.classList.remove('active');
        });
    }

    sliderModal();
}

