
var galleryThumbss = new Swiper('.gallery-thumbs', {
    direction: 'vertical',
    spaceBetween: 10,
    slidesPerView: 3,
    breakpoints: {
      // when window width is >= 320px
      300: {
        loop: true,
        slidesPerView: 2,
        spaceBetween: 10,
        direction: 'horizontal',
      },
      320: {
        loop: true,
        slidesPerView: 2,
        spaceBetween: 10,
        direction: 'horizontal',
      },
      // when window width is >= 480px
      480: {
        loop: true,
        slidesPerView: 2,
        spaceBetween: 10,
        direction: 'horizontal',
      },
  
      1000: {
        direction: 'vertical',
        slidesPerView: 3,
        spaceBetween: 10,
      },
      1300: {
        direction: 'vertical',
        slidesPerView: 3,
        spaceBetween: 10,
      },
      1500: {
        direction: 'vertical',
        slidesPerView: 3,
        spaceBetween: 10,
      },
      1940: {
        direction: 'vertical',
        slidesPerView: 3,
        spaceBetween: 10,
      },
      2240: {
        direction: 'vertical',
        slidesPerView: 3,
        spaceBetween: 10,
      },
    },
    navigation: {
      nextEl: '.swiper-button-next',
      prevEl: '.swiper-button-prev',
    },
  
  });
  var galleryTop = new Swiper('.gallery-top', {
    spaceBetween: 0,
  
    slidesPerView: 1,
    thumbs: {
      swiper: galleryThumbss,
    },
  });
  