new Swiper('.swiper-container', {
    loop: false,
    autoplay: {
      delay: 5000,
    },

    grabCursor: true,
    centeredSlides: true,
    slidesPerView: 4,

    breakpoints: {
      // when window width is >= 320px
      320: {
        slidesPerView: 4,
        spaceBetween: 0,
      },
      // when window width is >= 480px
      480: {
        slidesPerView: 4,
        spaceBetween: 0,
      },

      1500: {
        slidesPerView: 4,
        spaceBetween: 0,
      },
      1940: {
        slidesPerView: 4,
        spaceBetween: 0,
      },
    },


});
new Swiper('.swiper-container-2', {
    loop: false,
    grabCursor: true,
    slidesPerView: 2,

    breakpoints: {
      // when window width is >= 320px
      300: {
        slidesPerView: 2.2,
        spaceBetween: 0,
      },
      320: {
        slidesPerView: 2.22,
        spaceBetween: 0,
      },
      // when window width is >= 480px
      480: {
        slidesPerView: 2.5,
        spaceBetween: 0,
      },

      1500: {
        slidesPerView: 0,
        spaceBetween: 0,
      },
      1940: {
        slidesPerView: 0,
        spaceBetween: 0,
      },
    },


});
new Swiper('.swiper-container-group', {
    loop: false,
    grabCursor: true,
    slidesPerView: 5.5,
    navigation: {
      nextEl: '.swiper-button-next',
      prevEl: '.swiper-button-prev',
    },
    breakpoints: {
      // when window width is >= 320px
      300: {
        slidesPerView: 2.3,
        spaceBetween: 10,
      },
      320: {
        slidesPerView: 2.22,
        spaceBetween: 10,
      },
      // when window width is >= 480px
      480: {
        slidesPerView: 2.5,
        spaceBetween: 10,
      },

      1000: {
        slidesPerView: 3.5,
        spaceBetween: 0,
      },
      1300: {
        slidesPerView: 4.5,
        spaceBetween: 0,
      },
      1500: {
        slidesPerView: 5.5,
        spaceBetween: 0,
      },
      1940: {
        slidesPerView: 5.5,
        spaceBetween: 0,
      },
    },


});
new Swiper('.swiper-container-page', {
    loop: false,
    grabCursor: true,
    slidesPerView: 3.5,
    navigation: {
      nextEl: '.swiper-button-next',
      prevEl: '.swiper-button-prev',
    },
    breakpoints: {
      // when window width is >= 320px
      300: {
        slidesPerView: 1.8,
        spaceBetween: 10,
      },
      320: {
        slidesPerView: 1.8,
        spaceBetween: 10,
      },
      // when window width is >= 480px
      480: {
        slidesPerView: 2,
        spaceBetween: 10,
      },

      1000: {
        slidesPerView: 3.5,
        spaceBetween: 20,
      },
      1300: {
        slidesPerView: 3.5,
        spaceBetween: 20,
      },
      1500: {
        slidesPerView: 3.5,
        spaceBetween: 25,
      },
      1940: {
        slidesPerView: 3.5,
        spaceBetween: 30,
      },
    },


});



var galleryTopMap = new Swiper('.gallery-top-map', {

  navigation: {
    nextEl: '.swiper-button-next',
    prevEl: '.swiper-button-prev',
  },
  freeMode: true,
  pagination: {
    el: '.swiper-pagination-map',
    clickable: true,
  },
  speed: 1000,
  slideToClickedSlide: true,
  preventClicks: true,
  centeredSlides: true,
  breakpoints: {
    // when window width is >= 320px
    300: {
      slidesPerView: 1.5,
      spaceBetween: 10,
    },
    320: {
      slidesPerView: 1.5,
      spaceBetween: 10,
    },
    // when window width is >= 480px
    480: {
      slidesPerView: 2,
      spaceBetween: 10,
    },
    680: {
      slidesPerView: 3,
      spaceBetween: 0,
    },
    880: {
      slidesPerView: 4,
      spaceBetween: 0,
    },

    1000: {
      slidesPerView: 3.5,
      spaceBetween: 0,
    },
    1300: {
      slidesPerView: 4.5,
      spaceBetween: 0,
    },
    1500: {
      slidesPerView: 5.5,
      spaceBetween: 0,
    },
    1940: {
      slidesPerView: 5.5,
      spaceBetween: 0,
    },
  },


});

